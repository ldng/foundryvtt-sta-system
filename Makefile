build: _build/system.json

_build:
	mkdir -p $@
_build/system.json: _build
	cp -a src/* _build/
	rm _build/system.json
	test -n "$(TAG)"
	test -n "$(MANIFEST)"
	test -n "$(PKG_U)"
	echo "$(PWD)"
	cat src/system.json | jq --arg tag "$(TAG)" '.version = $$tag' |\
	jq --arg manifest "${MANIFEST}" '.manifest = $$manifest' |\
	jq --arg pkg_u "${PKG_U}" '.download = $$pkg_u' > $@
	if [ -n "$(CI_PROJECT_URL)" ]; then \
		mv $@ $@.n ; \
		jq < $@.n --arg ci_project_url "${CI_PROJECT_URL}" '.url = $$ci_project_url' > $@ ;\
		rm -f $@.n ; \
	fi
clean:
	rm -Rf _build
